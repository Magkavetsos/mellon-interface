function addComment(){
    let request = prompt("Please enter your comment:", "comment title");
    if(request == null || request == "") {
        return;
    }else{
        let para = document.createElement("p");
        para.innerHTML = request;
        para.classList.add("comment");
        let find_active_ticket = document.getElementById("ticket-list").getElementsByClassName("active")[0].className.split(' ');
        for (let y = 0; y < find_active_ticket.length; y++){
            if(find_active_ticket[y] !== "active" && find_active_ticket[y] !== "display-block" && find_active_ticket[y] !== "display-none" && find_active_ticket[y].includes("ticket")){
                para.classList.add(find_active_ticket[y]);
            }
        }
        let element = document.getElementById("subject");
        element.appendChild(para);
    }
}

function createRequest(){
    let request = prompt("Please enter your project request:", "Request title");
    if (request == null || request == "") {
        return;
    } else {
        let list_item = document.createElement("li");
        document.getElementById("project-list").appendChild(list_item);
         let link_item = document.createElement("a");
        list_item.appendChild(link_item);
        link_item.innerHTML += request;
        link_item.href = "#";
        var count = document.getElementById("project-list").getElementsByTagName("a").length;
        link_item.classList.add(String.fromCharCode(96 + count));
        changeActive();
    }
}

//count number of Projects - Change active class onclick
var header = document.getElementById("projects");
var link = header.getElementsByTagName("a");
changeActive();

function changeActive() {
    for (let i = 0; i < link.length; i++) {
        link[i].addEventListener("click", function () {
            let current = document.getElementsByClassName("active");
            current[0].classList.remove("active");
            this.classList.add("active");
            change_ticket_view();
        });
    }
}
function change_ticket_view() {
    var asd = document.getElementById("project-list").getElementsByTagName("a");
    for (let i = 0; i < asd.length; i++) {
        if(asd[i].classList.contains("active")){
            let active_classes = asd[i].className.split(' ');
            for (let y = 0; y < active_classes.length; y++){
                if(active_classes[y] !== "active"){
                    var project_number = active_classes[y];
                }
            }
        }
    }
    //now lets reveal the right tickets for active project_number
    var ticket_list = document.getElementById("ticket-list").getElementsByTagName("a");
            for (let i = 0; i < ticket_list.length; i++){
                if(ticket_list[i].classList.contains(project_number)){
                    ticket_list[i].classList.remove("display-none");
                    ticket_list[i].classList.add("display-block");
                }else{
                    ticket_list[i].classList.add("display-none");
                    ticket_list[i].classList.remove("display-block");
                }
            }
}

function createTicket(){
    let ticket = prompt("Please enter your ticket:", "Ticket title");
    if (ticket == null || ticket == "") {
        return;
    } else {
        let list_item = document.createElement("li");
        document.getElementById("ticket-list").appendChild(list_item);
        let link_item = document.createElement("a");
        list_item.appendChild(link_item);
        link_item.innerHTML += ticket;
        link_item.href = "#";
        //give the ticket the right class refers to project
        let number_of_tickets_created = document.getElementById("ticket-list").getElementsByTagName("li").length;
        let active_project_ticket = document.getElementById("project-list").getElementsByTagName("a");
        for (let i = 0; i < active_project_ticket.length; i++) {
            if(active_project_ticket[i].classList.contains("active")){
                let active_class = active_project_ticket[i].className.split(' ');
                for (let y = 0; y < active_class.length; y++){
                    if(active_class[y] !== "active"){
                        let new_ticket_class = active_class[y];
                        link_item.classList.add(new_ticket_class);
                    }
                }
            }
        }
        link_item.classList.add("ticket"+number_of_tickets_created);
        changeActive();
        changeTicketActive();
    }
}

var ticket_header = document.getElementById("tickets");
var ticket_link = ticket_header.getElementsByTagName("a");
changeTicketActive();

function changeTicketActive() {
    for (let i = 0; i < ticket_link.length; i++) {
        ticket_link [i].addEventListener("click", function () {
            let current = ticket_header.getElementsByClassName("active");
            current[0].classList.remove("active");
            this.classList.add("active");
        });
    }
}

var theParent = document.querySelector("#ticket-list");
theParent.addEventListener("click", changeSubjectDisplay, false);

    function changeSubjectDisplay(e) {
        if (e.target !== e.currentTarget) {
            if(document.getElementById("subjects").classList.contains("display-none")){
                document.getElementById("subjects").classList.remove("display-none");
                document.getElementById("subjects").classList.add("display-block");
            }
            e = e || window.event;
            var target = e.target || e.srcElement,
                text = target.textContent || target.innerText;
            var class_of_clicked_ticket = target.className.split(' ');
            for (let y = 0; y < class_of_clicked_ticket.length; y++){
                if(class_of_clicked_ticket[y] !== "active" && class_of_clicked_ticket[y] !== "display-block" && class_of_clicked_ticket[y] !== "display-none" && class_of_clicked_ticket[y].includes("ticket")){
                    // para.classList.add(find_active_ticket[y]);
                    class_of_clicked_ticket = class_of_clicked_ticket[y];
                }
            }
            var active_comment = document.getElementById("subject").getElementsByClassName("comment");
            for (let y = 0; y < active_comment.length; y++){
                if(active_comment[y].classList.contains(class_of_clicked_ticket)){
                    active_comment[y].classList.add("display-block");
                    active_comment[y].classList.remove("display-none");
                }else{
                    active_comment[y].classList.add("display-none");
                    active_comment[y].classList.remove("display-block");
                }
            }
            var subjecttitle = document.getElementById("subjects");
            var mainsubjecttitle = subjecttitle.getElementsByClassName("subject-title");
            mainsubjecttitle[0].textContent = text;
            return;
        }
        e.stopPropagation();
    }

var active_project_on_start = document.getElementById("project-list").getElementsByClassName("active")[0];
var project_click = document.querySelector("#project-list");
project_click.addEventListener("click", change_subject_view, false);

function change_subject_view(e){
    if (e.target !== e.currentTarget) {
        e = e || window.event;
        var target = e.target || e.srcElement,
            text = target.textContent || target.innerText;
        if(target.classList.contains("active") && target !== active_project_on_start){
            var check_active_ticket = document.getElementById("ticket-list").getElementsByClassName("active");
            if(!check_active_ticket[0].classList.contains("display-block")){
                document.getElementById("subjects").classList.add("display-none");
                active_project_on_start = target;
            }else{
                document.getElementById("subjects").classList.remove("display-none");
                document.getElementById("subjects").classList.add("display-block");
            }


        }
        return;
    }
    e.stopPropagation();

}
